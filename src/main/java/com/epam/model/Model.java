package com.epam.model;

import java.util.Map;

public interface Model {
    String addWindowCleaner();
    String addDishCleaner();
    String addFloorCleaner();
    Map<String,Integer> outputShoppingCart();
}
