package com.epam.model;

import java.util.HashMap;
import java.util.Map;

public class AddToShoppingCart {
    static Map<String,Integer> addChemistry = new HashMap<>();

    public static void addToShoppingCart(String typeOfChemistry, int price) {
        addChemistry.put(typeOfChemistry, price);
    }

    public static Map<String,Integer> returnShoppingCart() {
        return addChemistry;
    }
}
