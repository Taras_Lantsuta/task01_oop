package com.epam.model;

import java.util.Map;
import com.epam.view.Chemistry;
import static com.epam.view.Chemistry.*;

public class MyModel implements Model {

    private Chemistry window = Chemistry.valueOf(String.valueOf(WINDOWS));
    private Chemistry dish = Chemistry.valueOf(String.valueOf(DISHES));
    private Chemistry floor = Chemistry.valueOf(String.valueOf(FlOOR));

    public String addWindowCleaner() {
        AddToShoppingCart.addToShoppingCart(getTypeOfChemistry(WINDOWS), window.getPrice());
        return "Додано засіб для очищення вікон!";
    }

    public String addDishCleaner() {
        AddToShoppingCart.addToShoppingCart(getTypeOfChemistry(DISHES), dish.getPrice());
        return "Додано засіб для очищення посуду!";
    }

    public String addFloorCleaner() {
        AddToShoppingCart.addToShoppingCart(getTypeOfChemistry(FlOOR), floor.getPrice());
        return "Додано засіб для очищення підлоги!";
    }

    public Map<String,Integer> outputShoppingCart() {
        return AddToShoppingCart.returnShoppingCart();
    }
}
