package com.epam.controller;

import com.epam.model.MyModel;
import java.util.Map;

public class MyController implements Controller {
    private MyModel model;

    public MyController() {
        model = new MyModel();
    }

    @Override
    public String addDishCleaner() {
        return model.addDishCleaner();
    }

    @Override
    public String addFloorCleaner() {
        return model.addFloorCleaner();
    }

    @Override
    public String addWindowCleaner() {
        return model.addWindowCleaner();
    }

    @Override
    public Map<String,Integer> outputShoppingCart() {
        return model.outputShoppingCart();
    }
}
