package com.epam.controller;

import java.util.Map;

public interface Controller {
    String addWindowCleaner();
    String addDishCleaner();
    String addFloorCleaner();
    Map<String,Integer> outputShoppingCart();
}
