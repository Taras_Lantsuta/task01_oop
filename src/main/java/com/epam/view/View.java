package com.epam.view;

@FunctionalInterface

public interface View {
    void print();
}
