package com.epam.view;

import com.epam.controller.MyController;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyMenu {
    private MyController controller;
    private Map<String, String> menu;
    private Map<String, View> menuMethods;
    private static Scanner input = new Scanner(System.in);

    public MyMenu() {
        controller = new MyController();
        menu = new LinkedHashMap<>();
        menu.put("1", "Додати засіб для миття вікон: 1");
        menu.put("2", "Додати засіб для миття посуду: 2");
        menu.put("3", "Додати засіб для миття підлоги: 3");
        menu.put("4", "Вивести кошик за ціною: 4");
        menu.put("5", "Закінчити покупки: 5");

        menuMethods = new LinkedHashMap<>();
        menuMethods.put("1", this::pressButton1);
        menuMethods.put("2", this::pressButton2);
        menuMethods.put("3", this::pressButton3);
        menuMethods.put("4", this::pressButton4);
    }
    private void pressButton1(){ System.out.println(controller.addWindowCleaner()); }
    private void pressButton2(){ System.out.println(controller.addDishCleaner()); }
    private void pressButton3(){ System.out.println(controller.addFloorCleaner()); }
    private void pressButton4(){ System.out.println(controller.outputShoppingCart()); }

    private void outputMenu() {
        System.out.println("\nМеню:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            keyMenu = input.nextLine().toUpperCase();
            try {
                menuMethods.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("5"));
    }
}
