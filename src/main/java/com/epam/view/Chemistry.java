package com.epam.view;

public enum Chemistry {
    WINDOWS(15),
    FlOOR(30),
    DISHES(20);

    public int price;
    Chemistry(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public static String getTypeOfChemistry(Chemistry type) {
        String typeof = "Не визначений";
        switch (type) {
            case FlOOR:
                typeof = "Миючий засіб для підлоги";
                break;
            case DISHES:
                typeof = "Миючий засіб для тарілок";
                break;
            case WINDOWS:
                typeof = "Миючий засіб для вікон";
                break;
            default:
        }
        return typeof;
    }
}
